/* Basic Library for C/C++ (blclib)
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (2011 - 2015)
 
 Author: Arnaud Blanchard
 
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
  users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
  In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
  that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
  and, more generally, to use and operate it in the same conditions as regards security.
  The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms. */

/*
 The usage formating follows the format of argparse in Python ( https://docs.python.org/3/library/argparse.html ).
 Otherwise, we can be inspired from http://docopt.org
 */

/**
 @defgroup blc_program Program
 Functions to parse and manage arguments
  @{*/

#ifndef BLC_PROGRAM_H
#define BLC_PROGRAM_H

#include "blc_core.h"
#include "blc_command.h"

START_EXTERN_C

///Set the desciption of the program used for help. A copy of desciption is done (strdup).
void blc_program_set_description(char const *description);

/** Add parameter required or not after the program name.
required_nb:positif number a fix number of parameters, 0 for facultatif parameter
results ist a null terminated list of parameters. ( like argv)
 */
void blc_program_add_parameter(char const **results, char const *name, int required_nb, char const *help, char const *default_value);

void blc_program_add_multiple_parameters(char ***result_list, char const *name, int required_nb, char const *help);


//void blc_program_add_channel_parameter(blc_channel *channel, int required_nb, char const *help);
//void blc_program_add_channel_option(blc_channel *channel, char letter, char const *long_option, char const *help, char const* default_value);
void blc_program_add_input_pipe_option(FILE *file, char letter, char const *long_option, char const *help, char * default_value);
void blc_program_add_output_pipe_option(FILE *file, char letter, char const *long_option, char const *help, char * default_value);
//void blc_program_add_channel_parameter(blc_channel *channel, int required_nb, char const *help);

/**Add a possible option to the program. 
 The argument following the option, if any, will be put in result otherwise "1" will be put in result.
 */
void blc_program_add_option( char  const**result, char letter, char const *long_option, char const *parameter_type, const char *help, char const* default_value);

///Interpret the program arguments and update the results values as it should.
///The program name will be set in **blc_program_name**.
void blc_program_parse_args(int *argc, char ***argv);

///Do program_option_interpret but print the name of the program (argv[0]) underlined with '='.
void blc_program_parse_args_and_print_title(int *argc, char ***argv);

///Display the different possible argument for the  program.
void blc_program_args_display_help();

/** Start a textual program.
 * This has to be called after defining the parameters of the program
 * - Parse the arguments of the programs
 * - display the title
 * - check the color possibility of the terminal stderr
 * - eventually your 'exit_cb' if it not NULL.
 * - set internal variables setting if the input and output terminal are tty*/
void blc_program_init(int *argc, char ***argv, void(*exit_cb)(void));

/** Check wether threre is no more argument to parse. Otherwise publish information and quit. 
 This should be used after 'blc_program_init',  'blc_program_option_interpret', or 'blc_program_option_interpret_and_print_title' if you do not plan to parse them yourself.*/
void blc_program_check_full_parsing(int argc, char **argv);

/** Stop a textual program
 * - Send a quitting message with the name of the app on stderr.
 * - quit with no error (0).
 */
void blc_quit();

END_EXTERN_C
#endif
///@}
