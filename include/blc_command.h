/* Basic Library for C/C++ (blclib)
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (2011 - 2015)
 
 Author: Arnaud Blanchard
 
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
  users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
  In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
  that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
  and, more generally, to use and operate it in the same conditions as regards security.
  The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms. */

/*
 The usage formating follows the format of argparse in Python ( https://docs.python.org/3/library/argparse.html ).
Otherwise, we can be inspired from http://docopt.org
 */

/**
@defgroup blc_command command
Few functions helping for pseudo realtime applications.
@{*/

#ifndef BLC_COMMAND_H
#define BLC_COMMAND_H

#include "blc_tools.h"

/**
 period in microseconds
 0 for system as fast as possible, -1 for blocking on keyborad, -2 blocking after first iteration.
 Warning the period can be till 10ms longer than, asked. Problem of usleep/nano sleep */
#define BLC_COMMAND_LOOP(period) for(blc_command_loop_init(period); blc_command_loop_start();blc_command_loop_end())

typedef enum {BLC_QUIT=0, BLC_RUN, BLC_PAUSE, BLC_STEPS} type_blc_status;
typedef void (*type_blc_command_cb)(char const *argument, void *user_data);

extern int blc_input_terminal, blc_output_terminal;
extern type_blc_status blc_status;
extern uint64_t blc_loop_iteration;
extern uint64_t blc_loop_iteration_limit;
extern long blc_command_loop_period;
extern FILE *blc_profile_file;


START_EXTERN_C

//void * to not need to include "semaphore.h"
void blc_loop_sem_to_unlock_on_stop(void *sem);

void blc_command_ask_quit();

void pause_cb();


///Add a command in the list of possible command lines from the user.
void blc_command_add(const char *command_name, type_blc_command_cb callback, const char *prompt, const char *help, void *user_data);

/**If the command is the format of a blc_channel, the command is sent to stdout and the command is not interpreted.
 /usr, /bin etc. may be interpreted as a blc_channel. use /usr/,  /bin/ in this case.
 Same for any command starting with . ^ / :
 */
void blc_command_forward_blc_channels();


///Remove a previously added command
void blc_command_remove(const char *command_name);

///Interpret the command passed in string.
void blc_command_interpret_string(char const *input_string, size_t size);

///Wait a input from the user (this id blocking) and interprets it depending on the blc_command list.
void blc_command_interpret();

///Like blc_command_interpret but does not block if there is nothing to read (return 0 in this case).
int blc_command_try_to_interpret();

//May not be useful
void *blc_command_interpret_loop(char const *option);

///Start a thread listinning to the user entry without blocking.
void blc_command_interpret_thread(char const *option, void (*ask_quitting_funtion)());

void blc_command_interpret_block();

///Display the list of blc_command with help.
void blc_command_display_help();

void blc_command_update_float_cb(char const *argument, void *float_pt);
void blc_command_update_double_cb(char const *argument, void *double_pt);
void blc_command_update_int_cb(char const *argument, void *int_pt);

int blc_loop_try_add_waiting_semaphore(void *semaphore);
int blc_loop_try_add_posting_semaphore(void *semaphore);

/**The loop will start only once the semaphore is available.
 The waiting time does not count is the loop timing.
 When quitind these semaphores will be posted to be unlock the waiting loop
 We use void* instead of sem_t* to not force every body to include semaphore.h*/
void blc_loop_add_waiting_semaphore(void *semaphore);

/**In the end the loop, this sempahore will be posted
 We use void* instead of sem_t* to not force every body to include semaphore.h*/
void blc_loop_add_posting_semaphore(void *semaphore);

/**The loop willt start only once the waiting_callback callback will be executed. 
 The waiting time does not count is the loop timing*/
void blc_loop_add_waiting_callback( void(*waiting_callback)(void*), void *user_data);
/** Loop period in ms.
- 0 the system is as fast as possible
- -1 it blocks on keyboard
- -2 it blocks on keyboard after a first iteration*/
void blc_command_loop_init(long loop_period);
int blc_command_loop_start();
void blc_command_loop_end();

/**Wait until the blc_loop_stop. 
 Return the loop_thred pointer (always NULL for now)*/
void *blc_loop_wait_stop();

/** Stop a textual program
 * - Send a quitting message with the name of the app on stderr.
 * - Send 'q' and flush on stdout if it is a piped output
 * - quit with no error (0).
 */
void blc_quit();

END_EXTERN_C
#endif
///@}
