/* Basic Library for C/C++ (blclib)
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (2011 - 2014)
 
 Author:  Arnaud Blanchard
 
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
  users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
  In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
  that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
  and, more generally, to use and operate it in the same conditions as regards security.
  The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms. */

#include "blc_command.h"
#include "blc_realtime.h" //us_

#include <unistd.h>
#include <fcntl.h>
#include <pthread.h>
#include <termios.h>
#include <getopt.h>
#include <signal.h>
#include <errno.h> //errno and EINT
#include <libgen.h> //basename
#include <unistd.h> //usleep
#include <sys/wait.h>
#include <sys/ioctl.h>
#include <sys/select.h>

#include "blc_text.h"
#include "blc_tools.h"
#include "blc_program.h"


//Identical to argparse in Python
#define POSITIONAL_ARGUMENTS_TITLE "\npositional arguments:\n"
#define OPTIONAL_ARGUMENTS_TITLE "\noptional arguments:\n"

typedef void*(*type_pthread_cb)(void*);

typedef struct
{
    const char *name;
    type_blc_command_cb callback;
    const char *prompt;
    const char *help;
    void *user_data;
}blc_command;


// Static means it is only existing in this file
static blc_command *blc_commands = NULL;
static int blc_commands_nb = 0;

static sem_t *sems_to_unlock=NULL;
static int sems_to_unlock_nb=0;

static int forward_blc_channel=0;

void blc_loop_sem_to_unlock_on_stop(void *sem){
    
    APPEND_ITEM(&sems_to_unlock, &sems_to_unlock_nb, &sem);
}

START_EXTERN_C


void blc_command_forward_blc_channels(){
    forward_blc_channel=1;
}

void blc_command_display_help()
{
    const blc_command *command;
    size_t command_length_max = 0;
    size_t command_length=0;
    
    FOR_EACH(command, blc_commands, blc_commands_nb)
    {
        command_length = strlen(command->name);
        if (command->prompt) command_length+=strlen(command->prompt)+2;
        command_length_max = MAX(command_length, command_length_max);
    }
    
    FOR_EACH(command, blc_commands, blc_commands_nb)
    {
        if (command->prompt) fprintf(stderr, "%s<%s>%*c:%s\n", command->name, command->prompt, (int)(command_length_max - strlen(command->name)-strlen(command->prompt)-1), ' ', command->help);
        else    fprintf(stderr, "%-*s :%s\n", (int)command_length_max, command->name, command->help);
    }
}

void blc_command_add(const char *command_name, type_blc_command_cb callback, const char *prompt, const char *help, void *user_data){
    blc_command tmp_command;
    int i;
    
    tmp_command.name = command_name;
    tmp_command.callback = callback;
    tmp_command.prompt = prompt;
    tmp_command.help = help;
    tmp_command.user_data = user_data;
    
    FOR_INV(i, blc_commands_nb)
    if (strcmp(blc_commands[i].name, command_name) == 0) EXIT_ON_ERROR("%s: command '%s' is already defined.", blc_program_id,  command_name);
    
    APPEND_ITEM(&blc_commands, &blc_commands_nb, &tmp_command);
}


void blc_command_remove(const char *command_name){
    int i;
    
    FOR_INV(i, blc_commands_nb){
        if (strcmp(blc_commands[i].name, command_name) == 0) break;
    }
    if (i==-1) EXIT_ON_ERROR("You try to remove a command '%s' that does not exist", command_name);

    REMOVE_ITEM_POSITION(&blc_commands, &blc_commands_nb, i);
}

void blc_command_interpret_string(char const *input_string, size_t size){
    char const*parameter;
    char *parameter2;
    
    size_t parameter_size, parameter2_size;
    const blc_command *command;
    size_t name_size, line_capability=0;
    
    
    if (forward_blc_channel) {
        if (strchr("./^:", input_string[0])) //it starts with ., ^ , / , :, it may be a blc_channel.
            if (memchr(&input_string[1], '/', size)==0) // There is no / after the first char it is a blc_channel. NOTE: It may be an error with /usr /bin the user has to use /usr/ /bin/ etc.
            {
                
                SYSTEM_ERROR_CHECK(puts(input_string), -1, NULL);
                SYSTEM_ERROR_CHECK(fflush(stdout), -1, NULL);
                return;
            }
    }
    
    FOR_EACH(command, blc_commands, blc_commands_nb){
        name_size = strlen(command->name);
        if (strncmp(command->name, input_string, name_size) == 0){
            parameter = input_string + name_size;
            parameter_size = size - name_size;
            if (parameter_size == 0){ //No text after the command
                parameter2 = NULL;
                if ((command->prompt != NULL) && (command->prompt[0]!='|')) {//A text was expected
                    fprintf(stderr, "%s:", command->prompt);
                    parameter2_size = getline(&parameter2, &line_capability, stdin)-1;
                    parameter2[parameter2_size]=0;
                }
                if (command->callback == NULL) EXIT_ON_ERROR("Command callback is NULL for command '%s'", command->name);
                command->callback(parameter2, command->user_data);
                FREE(parameter2);
                line_capability=0;
                break; //Command has been found
            }
            else{
                //A parameter was expected
                if (command->prompt)   {
                    command->callback(parameter, command->user_data);
                    break;
                }
                else continue; //Maybe it is a command with a longer name, we continue to check
            /*    if ((command->prompt != NULL) && (command->prompt[0]!='|')) continue; // If we do not wait for parameter, the command must be exact.
                else
                return;*/
            }
        }
    }
    if (command == blc_commands+blc_commands_nb) fprintf(stderr, "%s: unknown command in: '%s'\n", blc_program_id,  input_string);
}

void blc_command_interpret_block(){
    char *line=NULL;
    size_t line_capability=0;
    ssize_t line_size;
    
    //We do not consider error if it was an interruption. @TODO check wether the content of lin is correct.
    do{
        line_size = getline(&line, &line_capability, stdin);
    }while ((line_size==-1) && (errno==EINTR));
    
    if (line_size==-1)
    {
        if ((errno==0) || (errno==ETIMEDOUT) || (errno==EINVAL)) {
            if (line) FREE(line);
            blc_command_ask_quit(); //stdin closed
        }
        else EXIT_ON_SYSTEM_ERROR("getline(&line, &line_capability, stdin)");
    }
    else{
        line_size=line_size-1;
        line[line_size]=0; // Removing the last return char (\n)
        blc_command_interpret_string(line, line_size);
    }
    FREE(line);
}

void blc_command_interpret(){
    fd_set rfds;
    int retval=-1;
    
    /* Surveiller stdin (fd 0) en attente d'entrées */
    FD_ZERO(&rfds);
    FD_SET(0, &rfds);
    
    //Block until there is data or there is an interuption (Ctrl+C, Terminate, ..)  which is not the case with getline.
    while (retval==-1){
        retval = select(1, &rfds, NULL, NULL, NULL);
        if (retval!=-1) blc_command_interpret_block();
        else if (errno!=EINTR) EXIT_ON_SYSTEM_ERROR("Waiting to interpret a command");
    }
}

int blc_command_try_to_interpret(){
    fd_set rfds;
    struct timeval time_val={0,0};
    int retval;
    
    /* Surveiller stdin (fd 0) en attente d'entrées */
    FD_ZERO(&rfds);
    FD_SET(STDIN_FILENO, &rfds);
    
    SYSTEM_ERROR_CHECK(retval = select(STDIN_FILENO+1, &rfds, NULL, NULL, &time_val), -1, NULL);
    if (retval) blc_command_interpret_block();
    else return 0;
    
    return 1;
}


/// if the parameter is not null we should display the help at each command.
static void *command_thread_interpret_loop(void *){
    while(blc_status==BLC_RUN){
        blc_command_interpret();
    }
    return NULL;
}


void blc_command_interpret_thread(char const *option, void (*ask_quit_funcion)()){
    pthread_t thread;
    
    if (ask_quit_funcion==NULL) ask_quit_funcion=blc_command_ask_quit;
    blc_command_loop_period=-2;
    if (strchr(option, 'h')) blc_command_add("h",  (type_blc_command_cb)blc_command_display_help, NULL, "display this help", NULL);
    if (strchr(option, 'q')) blc_command_add("q",  (type_blc_command_cb)ask_quit_funcion, NULL, "quit the application", NULL);
    SYSTEM_ERROR_CHECK(pthread_create(&thread, NULL, command_thread_interpret_loop, NULL), -1, NULL);
}

void blc_command_update_int_cb(char const *argument, void *int_pt){
    char *endptr;
    int value;
    
    if (argument==NULL || argument[0]==0) fprintf(stderr, "%d\n", *(int*)int_pt);
    else{
        value=strtod(argument, &endptr);
        if (endptr!=argument+strlen(argument)) fprintf(stderr, "Error reading '%s' as an int. The value is still:%d\n", argument,  *(int*)int_pt);
        else *(int*)int_pt=value;
    }
}

void blc_command_update_float_cb(char const *argument, void *float_pt){
    char *endptr;
    float value;
    
    if (argument==NULL || argument[0]==0) fprintf(stderr, "%f\n", *(float*)float_pt);
    else{
        value=strtof(argument, &endptr);
        if (endptr!=argument+strlen(argument)) fprintf(stderr, "Error reading '%s' as a float. The value is still:%f\n", argument,  *(float*)float_pt);
        else *(float*)float_pt=value;
    }
}

void blc_command_update_double_cb(char const *argument, void *double_pt){
    char *endptr;
    double value;
    
    if (argument==NULL || argument[0]==0) fprintf(stderr, "%lf\n", *(double*)double_pt);
    else{
        value=strtod(argument, &endptr);
        if (endptr!=argument+strlen(argument)) fprintf(stderr, "Error reading '%s' as a double. The value is still:%lf\n", argument,  *(double*)double_pt);
        else *(double*)double_pt=value;
    }
}


END_EXTERN_C
