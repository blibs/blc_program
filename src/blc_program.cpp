/* Basic Library for C/C++ (blclib)
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (2011 - 2014)
 
 Author:  Arnaud Blanchard
 
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
  users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
  In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
  that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
  and, more generally, to use and operate it in the same conditions as regards security.
  The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms. */

#include "blc_program.h"
#include "program.hpp" // internal to blc
#include <sys/time.h>

#include <stdio.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <pthread.h>
#include <termios.h>
#include <sys/select.h>
#include <getopt.h>
#include <signal.h>
#include <errno.h> //errno and EINT
#include <libgen.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <getopt.h>

#include "blc_text.h"
#include "blc_tools.h"
#include "blc_command.h"

/**
 It manages arguments of programs and help
 */


typedef void*(*type_pthread_cb)(void*);

//typedef type_program_option_result;
enum {STRING, STRING_LIST, BLC_CHANNEL, INPUT_PIPE, OUTPUT_PIPE};

// This is a bit redundant with struct option but has we need to send an array of struct option to getopt_long, we need two independant arrays: one with struct option and one with type_program_option.
typedef struct {
    int type;
    union{
        //     blc_channel *channel;
        FILE *pipe;
        char const **string_pt;
    };
    char letter;
    char const *name;
    char const *parameter;
    char const *help;
    char const *default_value;
}type_program_option;

struct program_parameter{
    int type;
    union{
        //        blc_channel *channel;
        FILE *pipe;
        char  const **string_pt;
        char  ***string_list_pt;
        
    };
    char const *name;
    int required_nb;
    char const *help;
    char const *default_value;
};


FILE *blc_profile_file=NULL;
int blc_input_terminal, blc_output_terminal;

// Static means it is only existing in this file
static char const *program_description=NULL;
static type_program_option *blc_program_options=NULL;
static int blc_program_options_nb=0;
static struct program_parameter *blc_program_parameters=NULL;
static int blc_program_parameters_nb;

START_EXTERN_C

void blc_program_set_description(char const *description)
{
    program_description=description;
}

void blc_program_add_parameter(char const**result, char const *name, int required, char const *help, char const *default_value)
{
    struct program_parameter parameter;
    
    parameter.type=STRING;
    parameter.string_pt=result;
    parameter.name=name;
    parameter.required_nb=(required!=0); //At this moment only binary choices are possible
    parameter.help=help;
    parameter.default_value=default_value;

    APPEND_ITEM(&blc_program_parameters, &blc_program_parameters_nb, &parameter);
}

void blc_program_add_multiple_parameters(char ***result_list, char const *name, int required_nb, char const *help)
{
    struct program_parameter parameter;
    
    parameter.type=STRING_LIST;
    parameter.string_list_pt=result_list;
    parameter.name=name;
    parameter.required_nb=required_nb;
    parameter.help=help;
    APPEND_ITEM(&blc_program_parameters, &blc_program_parameters_nb, &parameter);
}

void blc_program_add_option(char const **result, char letter, char const *long_option, char const *parameter, char const *help,  char const* default_value){
    type_program_option  tmp_program_option;
    
   // if ((parameter) && (default_value)) EXIT_ON_ERROR("option '%s': you cannot required an parameter '%s' and having a default value '%s'", long_option, parameter, default_value);
    
    tmp_program_option.type = STRING;
    tmp_program_option.string_pt =  result;
    (*tmp_program_option.string_pt)=NULL;
    tmp_program_option.letter = letter;
    tmp_program_option.name = long_option;
    tmp_program_option.parameter = parameter;
    tmp_program_option.help = help;
    tmp_program_option.default_value = default_value;
    APPEND_ITEM(&blc_program_options, &blc_program_options_nb, &tmp_program_option);
}

void blc_program_add_input_pipe_option(FILE *file, char letter, char const *long_option, char const *help, char* default_value)
{
    type_program_option  tmp_program_option;
    
    tmp_program_option.type=INPUT_PIPE;
    tmp_program_option.pipe = file;
    tmp_program_option.letter = letter;
    tmp_program_option.name = long_option;
    tmp_program_option.parameter = "input_pipe";
    tmp_program_option.help = help;
    tmp_program_option.default_value=default_value;
    APPEND_ITEM(&blc_program_options, &blc_program_options_nb, &tmp_program_option);
}

void blc_program_add_output_pipe_option(FILE *file, char letter, char const *long_option, char const *help, char * default_value)
{
    type_program_option  tmp_program_option;
    
    tmp_program_option.type=OUTPUT_PIPE;
    tmp_program_option.pipe = file;
    tmp_program_option.letter = letter;
    tmp_program_option.name = long_option;
    tmp_program_option.parameter = "output_pipe";
    tmp_program_option.help = help;
    tmp_program_option.default_value=default_value;
    APPEND_ITEM(&blc_program_options, &blc_program_options_nb, &tmp_program_option);
}

static void blc_program_option_interpret(int *argc, char **argv[])
{
    char *optstring;
    int ret, option_index;
    int optstring_size=0;
    int options_nb=0;
    struct option *long_options = NULL, tmp_option;
    struct stat stat_data;
    type_program_option *program_option;
    char pipe_name[PATH_MAX+1];
    
    
    //We store enough room for all the optional letters + ':' + terminating null char
    optstring = MANY_ALLOCATIONS(blc_program_options_nb*2+1, char);
    
    //We define this list and initialise the result
    FOR_EACH_INV(program_option, blc_program_options, blc_program_options_nb)
    {
        optstring[optstring_size++]=program_option->letter;
        tmp_option.name = (const char*)program_option->name;
        if (program_option->parameter){
            tmp_option.has_arg=required_argument; //optional_argument does not work on OSX !!
        }
        else tmp_option.has_arg=no_argument;
        if (tmp_option.has_arg==required_argument) optstring[optstring_size++]=':'; //optional_argument does not work on OSX !!
        tmp_option.flag = NULL;
        tmp_option.val=program_option->letter;
        APPEND_ITEM(&long_options, &options_nb, &tmp_option);
    }
    // We stop the string and long_options array by a NULL value */
    optstring[optstring_size++]='\0';
    CLEAR(tmp_option);
    APPEND_ITEM(&long_options, &options_nb, &tmp_option);
    
    //We parse the command line
    while ((ret = getopt_long(*argc, *argv, optstring, long_options, &option_index)) != -1)
    {
        //Wrong option
        if (ret=='?') EXIT_ON_ERROR("Invalid program arguments. Use '%s --help' for help", blc_program_name);
        
        FOR_EACH_INV(program_option, blc_program_options, blc_program_options_nb)
        {
            if (ret == program_option->letter) //TODO see what to do with special char '?' and ':'
            {
                if (optarg == NULL) {
                    if (program_option->default_value) *program_option->string_pt=program_option->default_value;
                    else *program_option->string_pt="1";
                }
                else{
                    switch (program_option->type){
                        case STRING:
                            *program_option->string_pt = optarg;
                            break;
                            
                            //Pipe not in use anymore
                        case INPUT_PIPE:
                            SPRINTF(pipe_name, "/tmp/blc_pipes/%s", optarg);
                            SYSTEM_ERROR_CHECK(freopen(optarg, "r", program_option->pipe), NULL, NULL);
                            break;
                        case OUTPUT_PIPE:
                            mkdir("/tmp/blc_pipes", S_IRWXU);
                            SPRINTF(pipe_name, "/tmp/blc_pipes/%s", optarg);
                            if (mkfifo(pipe_name, S_IRWXU) !=0)
                            {
                                if (errno==EEXIST)
                                {
                                    SYSTEM_SUCCESS_CHECK(stat(pipe_name, &stat_data), 0, "Checking data of '%s'.", pipe_name);
                                    if (stat_data.st_mode == S_IFIFO){
                                        SYSTEM_ERROR_CHECK(freopen(pipe_name, "w", program_option->pipe), NULL, NULL);
                                    }
                                    else EXIT_ON_ERROR("Cannot use '%s' as a output pipe.", pipe_name);
                                }
                                else EXIT_ON_SYSTEM_ERROR("Creating output pipe '%s'.", pipe_name);
                            }
                            break;
                        default:EXIT_ON_ERROR("Type %d not managed.", program_option->type);
                    }
                }
            }
            if (ret=='h') { // We need to check it now otherwise, it may ask for parameter (blocking scanf)
                FREE(long_options);
                return;
            }
        }
    }
    FREE(long_options);
    
    //Set default values
    FOR_EACH_INV(program_option, blc_program_options, blc_program_options_nb)
    {
        switch (program_option->type){
            case STRING:
                if (*program_option->string_pt==NULL) *program_option->string_pt=(char*)program_option->default_value;
                break;
        }
    }
    (*argc)-=optind;
    (*argv)+=optind;
}


static void blc_program_interpret_parameters(int *argc, char **argv[]){
    char *tmp_parameter=NULL, *tmp_arg;
    int missing_parameters_nb;
    int i, j;
    size_t linecap;
    ssize_t parameter_read;
    
    FOR(i, blc_program_parameters_nb){
        switch(blc_program_parameters[i].type){
            case STRING :
                if ((*argc)==0){//There is no more argument to interpret
                    if (blc_program_parameters[i].required_nb){
                        if (isatty(STDIN_FILENO)) fprintf(stderr, "%s: %s? ", blc_program_parameters[i].help, blc_program_parameters[i].name);

                        parameter_read=getline(&tmp_parameter, &linecap, stdin);
                        if (parameter_read==-1){
                        	if (errno==ENOTTY) {
                        		color_eprintf(BLC_RED, "Quitting '%s': The standard input is not available. You probably have a program on an input pipe which has crashed\n", blc_program_id);
                        		exit(1);
                        	}
                        	else EXIT_ON_SYSTEM_ERROR("Reading input for parameter");
                        }
                        tmp_parameter[parameter_read-1]=0;//Remove the last return;
                        (*blc_program_parameters[i].string_pt)=tmp_parameter;
                        linecap=0;
                        tmp_parameter=NULL;
                        //     }else EXIT_ON_ERROR("Missing '%s' argument : %s", blc_program_parameters[i].name, blc_program_parameters[i].help);
                    }
                    else *blc_program_parameters[i].string_pt=blc_program_parameters[i].default_value;
                }
                else {
                    *blc_program_parameters[i].string_pt=(*argv)[0];
                    (*argc)--;
                    (*argv)++;
                }
                break;
                
            case STRING_LIST: EXIT_ON_ERROR("STRING LIST is not yet managed");
                if (isatty(STDIN_FILENO)) {
                    missing_parameters_nb = blc_program_parameters[i].required_nb-(*argc-optind-i);
                    if (missing_parameters_nb>0){
                        FOR(j, missing_parameters_nb){
                            fprintf(stderr, "%s: %s? ", blc_program_parameters[i].help, blc_program_parameters[i].name);
                            linecap=0;
                            SYSTEM_ERROR_CHECK(parameter_read=getline(&tmp_arg, &linecap, stdin), -1, "Reading input for parameter"); //getline(blc_program_parameters[i].string_pt, sizeof(blc_program_parameters[i].string_pt), stdin), NULL, "Wrong channel input name");
                            tmp_arg[parameter_read-1]=0;//Remove the last return;
                            APPEND_ITEM(argv, argc, &tmp_arg);
                        }
                        parameter_read=0;
                        APPEND_ITEM(argv, argc,  &parameter_read);
                        argc--;//The last NULL parameter does not count
                    }
                }
                else EXIT_ON_ERROR("Missing '%s' arguments : %s you must have %d arguments", blc_program_parameters[i].name, blc_program_parameters[i].help, missing_parameters_nb);
                *(blc_program_parameters[i].string_list_pt)=&(*argv)[i+optind];
                break;
        }
    }
}

void blc_program_parse_args(int *argc, char ***argv){
    blc_program_name=basename(*argv[0]);
    SYSTEM_ERROR_CHECK(asprintf((char**)&blc_program_id, "%s%d", blc_program_name, getpid()), -1, "Argv0: '%s'", *argv[0]);
    blc_program_option_interpret(argc, argv);
    blc_program_interpret_parameters(argc, argv);
}

void blc_program_parse_args_and_print_title(int *argc, char ***argv){
    
    underline_fprintf('=', stderr, basename(*argv[0]));
    blc_program_parse_args(argc, argv);
}

/*Display format as python argparse*/
void blc_program_args_display_help()
{
    struct program_parameter *parameter;
    int i, option_length_max, tmp_length=0;
    type_program_option *program_option;
    
    //conventions want the help on stderr (avoid to interfere with normal output)
    fprintf(stderr, "\nusage: %s", blc_program_name);
    option_length_max=0;
    
    FOR_EACH(program_option, blc_program_options, blc_program_options_nb)
    {
        if (program_option->letter)
        {
            fprintf(stderr, " [-%c", program_option->letter);
            tmp_length=3;
            if (program_option->parameter){
                fprintf(stderr, " %s", program_option->parameter);
                tmp_length+=strlen(program_option->parameter);
            }
            fprintf(stderr, "]");
        }
        option_length_max=MAX(option_length_max, (int)strlen(program_option->name)+tmp_length);
    }
    
    FOR_EACH(parameter, blc_program_parameters, blc_program_parameters_nb)
    {
        if (parameter->required_nb==0) fprintf(stderr, " [%s]", parameter->name);
        else if (parameter->required_nb==-1) fprintf(stderr, " %s ...", parameter->name);
        else if (parameter->required_nb==1) fprintf(stderr, " %s", parameter->name);
        else FOR(i, parameter->required_nb) fprintf(stderr, " %s%d", parameter->name, i+1);
        
        option_length_max=MAX(option_length_max, (int)strlen(parameter->name));
    }
    fprintf(stderr, "\n");
    
    if (program_description) fprintf(stderr, "\n%s\n", program_description);
    
    option_length_max+=10;
    if (blc_program_parameters_nb) fprintf(stderr, POSITIONAL_ARGUMENTS_TITLE);
    FOR_EACH(parameter, blc_program_parameters, blc_program_parameters_nb){
        fprintf(stderr, "  %-*s %s\n", option_length_max-2, parameter->name, parameter->help);
        if (parameter->default_value) fprintf(stderr, " (default: %s)\n", parameter->default_value);
    }
    
    fprintf(stderr, OPTIONAL_ARGUMENTS_TITLE);
    FOR_EACH(program_option, blc_program_options, blc_program_options_nb)
    {
        if (program_option->letter) {
            tmp_length=fprintf(stderr, " -%c", program_option->letter);
            if ( program_option->name) tmp_length+=fprintf(stderr, ",");
        }
        else tmp_length=0;
        if (program_option->name) tmp_length+=fprintf(stderr, " --%s", program_option->name);
        if (program_option->parameter) tmp_length+=fprintf(stderr, " %s", program_option->parameter);
        fprintf(stderr, "%*c", option_length_max - tmp_length, ' ');
        fprintf(stderr, "%s", program_option->help);
        if (program_option->default_value) {
       //     if (program_option->type==BLC_CHANNEL) fprintf(stderr, " (default: %s<pid>)\n", program_option->default_value);
       //     else
        	fprintf(stderr, " (default: %s)\n", program_option->default_value);
        }
        else fprintf(stderr, "\n");
    }
    fprintf(stderr, "\n");
}

static void on_sigterm(int){
    if (blc_status==BLC_QUIT){
        fprintf(stderr, "%s: receiving SIGTERM in quiting mode. Force to quit.\n", blc_program_id);
        exit(EXIT_FAILURE);
    }
    else {
        fprintf(stderr, "%s: receiving SIGTERM\n", blc_program_id);
        blc_command_ask_quit();
    }
}

static void on_sigtstp(int){
    fprintf(stderr, "%s: receiving SIGTSTP: pause\n", blc_program_id);
    blc_status=BLC_PAUSE;
}

static void on_sigcont(int){
    fprintf(stderr, "%s: receiving SIGCONT: continue\n", blc_program_id);
    blc_status=BLC_RUN;
}

static void on_sigint(int){
    if (blc_status==BLC_QUIT){
        fprintf(stderr, "%s: receiving SIGINT (Ctrl+C) in quiting mode. Force to quit.\n", blc_program_id);
        exit(EXIT_FAILURE);
    }
    else {
        fprintf(stderr, "%s :receiving SIGINT (Ctrl+C). Try to quit. Resend SIGINT if it does not quit.\n", blc_program_id);
        blc_status=BLC_QUIT;
    }
}

static void on_sigsegv(int){
    fprintf(stderr, "%s: receiving SIGSEGV segmentation fault ( memory access error )\n", blc_program_id);
    exit(EXIT_FAILURE);
}

//Normal exit is not called with value of 1 in exit
static void on_sigabrt(int ){
    fprintf(stderr, "%s: receiving SIGABORT ( error ! )\n", blc_program_id);
}

//At this moment error_exit_cb is the same than normal exit
void blc_program_init(int *argc, char ***argv, void (*exit_cb)(void))
{
    char  const*help, *profile;
    struct sigaction action;
    struct timeval timer;
    
    CLEAR(action);
    action.sa_handler = on_sigterm;
    SYSTEM_SUCCESS_CHECK(sigaction(SIGTERM, &action, 0), 0, NULL);
    
    action.sa_handler = on_sigtstp;
    SYSTEM_SUCCESS_CHECK(sigaction(SIGTSTP, &action, 0), 0, NULL);
    
    action.sa_handler = on_sigcont;
    SYSTEM_SUCCESS_CHECK(sigaction(SIGCONT, &action, 0), 0, NULL);
    
    action.sa_handler = on_sigint;
    SYSTEM_SUCCESS_CHECK(sigaction(SIGINT, &action, 0), 0, NULL);
    
    action.sa_handler = on_sigabrt;
    SYSTEM_SUCCESS_CHECK(sigaction(SIGABRT, &action, 0), 0, NULL);
    
    action.sa_handler = on_sigsegv;
    SYSTEM_SUCCESS_CHECK(sigaction(SIGSEGV, &action, 0), 0, NULL);
    
    blc_input_terminal=isatty(STDIN_FILENO);
    blc_output_terminal=isatty(STDOUT_FILENO);
    
    if (isatty(STDERR_FILENO) && blc_input_terminal) blc_stderr_ansi=1;
    else blc_stderr_ansi=0; //This is not waranty. It is safer to use blc_stderr_ansi_detect but it causes trouble when many program are launched at once
    
    blc_program_add_option(&profile, '~', "profile", "filename", "profile the execution of the loop", NULL);
    blc_program_add_option(&help, 'h', "help", 0, "display this help", NULL);
    blc_program_parse_args_and_print_title(argc, argv);
    
    if (profile){
        SYSTEM_ERROR_CHECK(blc_profile_file=fopen(profile, "w"), NULL, "filename: %s", profile);
        fprintf(blc_profile_file, "#start_time(µs) \texec_time(µs)    \tduration(µs)    \t\n");
        gettimeofday(&timer, NULL);
        fprintf(blc_profile_file, "%16lu\t               0\t       0\t\n", timer.tv_sec*1000000+timer.tv_usec);
    }
    
    if (help){
        blc_program_args_display_help();
        exit(EXIT_SUCCESS);
    }
    
    blc_program_check_full_parsing(*argc, *argv);
    if (exit_cb) atexit(exit_cb); //This will be called first
}

void blc_program_check_full_parsing(int argc, char **argv){
    int i;
    
    if (argc!=0) {
        blc_program_args_display_help();
        fprintf(stderr, "Unused arguments:");
        FOR(i, argc) fprintf(stderr, "'%s' ", argv[i]);
        fprintf(stderr, "\n");
        EXIT_ON_ERROR("Somme arguments are wrong, they have not been parsed.");
    }
}

void blc_quit()
{
    if (blc_profile_file) fclose(blc_profile_file);
    if (blc_program_id[0]) { //We test if it is not ""
        fprintf(stderr, "Quitting %s\n", blc_program_id);
        FREE(blc_program_id);
    }

}
END_EXTERN_C
