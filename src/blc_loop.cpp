
#include "blc_command.h"
#include "blc_realtime.h"
#include <unistd.h> //sleep
#include <errno.h> //errno
#include <math.h> //pthread_mutex

#include <time.h> //nanosleep
#include <sys/time.h> //gettimeofday
#include <pthread.h> //pthread_mutex

static struct timeval timer;
static uint blc_period=0;
static uint blc_duration=0;
static uint blc_duration_min=UINT_MAX, blc_duration_max=0;

static long blc_current_duration;
static int intermediate_iteration;

long blc_command_loop_period=-2; //unitialised
uint64_t blc_loop_iteration_limit=UINT64_MAX;
uint64_t blc_loop_iteration=0;

static void(**waiting_callbacks)(void*)=NULL;
static void **waiting_callabacks_user_data=NULL;
static int waiting_callbacks_nb=0;

static sem_t **waiting_semaphores;
static int waiting_semaphores_nb;

static sem_t **posting_semaphores;
static int posting_semaphores_nb;

static pthread_t loop_thread, loop_check_stuck_thread;
static pthread_mutex_t mutex_lock_keyboard=PTHREAD_MUTEX_INITIALIZER;

static int step_iteration;
static struct timeval step_timer, profile_timer;

static sem_t *blocking_semaphore=NULL;

#include "blc_program.h"

type_blc_status blc_status=BLC_RUN;

void blc_fprint_stats(FILE *file)
{
    if (intermediate_iteration){
        fprintf(file, "%-16.16s %4d iterations duration:%8.3fms[%8.3f, %8.3f] frequency:%8.3fHz period:%8.3fms\n", blc_program_id, intermediate_iteration, blc_duration/(1000.f*intermediate_iteration), blc_duration_min/1000., blc_duration_max/1000., 1e6*intermediate_iteration/(double)blc_period, blc_period/(1000.*intermediate_iteration));
        intermediate_iteration=0;
        blc_period=0;
        blc_duration=0;
        blc_duration_min=UINT_MAX;
        blc_duration_max=0;
    }
    else fprintf(file, "%s: No new iteration yet\n", blc_program_id);
    gettimeofday(&timer, NULL);
}

static void display_stats(){
    blc_fprint_stats(stderr);
}

void blc_loop_add_waiting_callback( void(*waiting_callback)(void*), void *user_data){

    MANY_REALLOCATIONS(&waiting_callbacks, waiting_callbacks_nb+1);
    MANY_REALLOCATIONS(&waiting_callabacks_user_data, waiting_callbacks_nb+1);
    
    waiting_callbacks[waiting_callbacks_nb]=waiting_callback;
    waiting_callabacks_user_data[waiting_callbacks_nb]=user_data;
    waiting_callbacks_nb++;
}

int blc_loop_try_add_waiting_semaphore(void *semaphore){
    if (semaphore==NULL) return 0;
    APPEND_ITEM(&waiting_semaphores, &waiting_semaphores_nb, &semaphore);
    return 1;
}

int blc_loop_try_add_posting_semaphore(void *semaphore){
    if (semaphore==NULL) return 0;
    APPEND_ITEM(&posting_semaphores, &posting_semaphores_nb, &semaphore);
    return 1;

}

void blc_loop_add_waiting_semaphore(void *semaphore){
    if (blc_loop_try_add_waiting_semaphore(semaphore)==0) EXIT_ON_ERROR("The semaphore you want to add is NULL");
}

void blc_loop_add_posting_semaphore(void *semaphore){
    if (blc_loop_try_add_posting_semaphore(semaphore)==0) EXIT_ON_ERROR("The semaphore you want to add is NULL");
}

//We send the argument to stdout if it is not a terminal
static void blc_command_send_to_stdout(char const *argument){
    if (blc_output_terminal==0){
    if (argument) SYSTEM_ERROR_CHECK(printf("%s", argument), -1, NULL);
    SYSTEM_ERROR_CHECK(printf("\n"), -1, NULL);
    SYSTEM_ERROR_CHECK(fflush(stdout), -1, NULL);
    }
}
//We execute the argument and send it with prefix '&' to stdout if it is not a terminal
static void blc_command_send_to_all(char const *argument){
    blc_command_interpret_string(argument, strlen(argument));
    if (blc_output_terminal==0){
    if (argument) SYSTEM_ERROR_CHECK(printf("&%s", argument), -1, NULL);
    SYSTEM_ERROR_CHECK(printf("\n"), -1, NULL);
    SYSTEM_ERROR_CHECK(fflush(stdout), -1, NULL);
    }
}

void blc_command_ask_quit(){
    if (blc_status==BLC_PAUSE) BLC_PTHREAD_CHECK(pthread_mutex_unlock(&mutex_lock_keyboard), NULL);
    
    if (blc_status!=BLC_STEPS) blc_status=BLC_QUIT;
    
    if (blocking_semaphore){
        if (sem_trywait(blocking_semaphore)==0){
            sem_post(blocking_semaphore); // we succeed, it was not a problem, I free it.
            blocking_semaphore=NULL;
        }
        else if (errno==EAGAIN) sem_post(blocking_semaphore);
        else EXIT_ON_SYSTEM_ERROR("unlocking semaphore for quitting");
    }
}

static void step_forward_cb(const char* steps_nb_str, void*){
    int  steps_nb, pos;
    if (steps_nb_str){
        if (sscanf(steps_nb_str, "%d%n", &steps_nb, &pos)!=1 || pos !=strlen(steps_nb_str) || steps_nb<1){
            color_eprintf(BLC_YELLOW, "'%s' is not a valid step number\n", steps_nb_str);
            return;
        }
        else {
            blc_status=BLC_STEPS;
            blc_loop_iteration_limit=blc_loop_iteration+steps_nb;
            blc_us_time_diff(&step_timer);
            BLC_PTHREAD_CHECK(pthread_mutex_unlock(&mutex_lock_keyboard), NULL);
        }
    }
    else {
        blc_status=BLC_STEPS;
        blc_loop_iteration_limit=blc_loop_iteration+1;
        blc_us_time_diff(&step_timer);
        BLC_PTHREAD_CHECK(pthread_mutex_unlock(&mutex_lock_keyboard), NULL);

    }
    step_iteration=blc_loop_iteration;
    blc_command_remove(">");
}

void pause_cb()
{
    if (blc_status==BLC_RUN || blc_status==BLC_STEPS){
        blc_status=BLC_PAUSE;
        blc_command_add(">", step_forward_cb, "|iterations", "continue for steps nb iterations", NULL);
        fprintf(stderr, "=== %s: pause iteration %lld ===  '' to continue, '>[<nb>]' to step one or run nb iterations ===\n", blc_program_id, blc_loop_iteration);
    }else if (blc_status==BLC_PAUSE){
        blc_command_remove(">");
        blc_status=BLC_RUN;
        fprintf(stderr, "=== %s: continue === \n", blc_program_id);
        BLC_PTHREAD_CHECK(pthread_mutex_unlock(&mutex_lock_keyboard), NULL);
    }
    else EXIT_ON_ERROR("Unknown status '%d'", blc_status);
}

static void *loop_check_stuck(void*){
    int iteration;
    int checking_time=5;
    int was_stucked=0;
    
    while(1){
        iteration = blc_loop_iteration;

        sleep(checking_time);
        if(blc_status==BLC_RUN){ //in BLC_PAUSE it is normal to block
        	if (iteration==blc_loop_iteration) {
            	color_eprintf(BLC_YELLOW, "'%s' seems blocked on iteration '%d'. The loop has been stopped for more than %ds\n", blc_program_id, iteration, checking_time);
                if (blc_profile_file) fflush(blc_profile_file); //It may help for debuggage
            	was_stucked=1;
        	}
        	else if (was_stucked) {
            	color_eprintf(BLC_GREEN, "'%s' program restart iteration '%d'.\n", blc_program_id, iteration);
            	was_stucked=0;
        	}
        }
    }
    return NULL;
}

/// if the parameter is not null we should display the help at each command.
static void *command_thread_interpret_loop(void *){
    
    while(blc_status!=BLC_QUIT){
        blc_command_interpret_block();
        if (blc_command_loop_period==-1) BLC_PTHREAD_CHECK(pthread_mutex_unlock(&mutex_lock_keyboard), NULL);
        //blc_command_interpret();
    }
    return NULL;
}

void blc_command_loop_init(long loop_period){
    if (blc_command_loop_period==-3) EXIT_ON_ERROR("You already are in a blc_command_loop");
    blc_loop_iteration=0;
    intermediate_iteration=0;
    blc_command_loop_period=loop_period;
    
    if (blc_command_loop_period==-2){
        blc_command_loop_period=-1;
    }
    else BLC_PTHREAD_CHECK(pthread_mutex_lock(&mutex_lock_keyboard), NULL);
    
    if (blc_input_terminal){
         blc_command_display_help(); //We display help only for keyboard
    }
    if (blc_command_loop_period!=-1) blc_command_add("", (type_blc_command_cb)pause_cb, NULL, "set on/off pause", NULL);

    blc_command_add("h", (type_blc_command_cb)blc_command_display_help, NULL, "display this help", NULL);
    blc_command_add("q", (type_blc_command_cb)blc_command_ask_quit, NULL, "quit the application", NULL);

    if (blc_command_loop_period!=-1) { //we lock on keyboard. But if the program lock we still read keyboard
        SYSTEM_ERROR_CHECK(pthread_create(&loop_check_stuck_thread, NULL, loop_check_stuck, NULL), -1, NULL);
        blc_command_add("s", (type_blc_command_cb)display_stats, NULL, "display time stats", NULL); //We did not do stat on keyboard
        fprintf(stderr, "=== %s: running ===\n", blc_program_id);
    }
    blc_command_add("|", (type_blc_command_cb)blc_command_send_to_stdout, "command", "send the command to stdout", NULL);
    blc_command_add("&", (type_blc_command_cb)blc_command_send_to_all, "command", "intepret caommdn and send it to stdout", NULL);

    SYSTEM_ERROR_CHECK(pthread_create(&loop_thread, NULL, command_thread_interpret_loop, NULL), -1, NULL);
}

int blc_command_loop_start(){
    
    int i;
    
    
    if (blc_profile_file) gettimeofday(&profile_timer, NULL);

    //We wait before counting the duration time as the time for waiting does not matter
    if (blc_loop_iteration==blc_loop_iteration_limit){
        fprintf(stderr, "%s: %d iterations executed in %.3fms\n", blc_program_id, (int)blc_loop_iteration-step_iteration, blc_us_time_diff(&step_timer)/1000.f);
        pause_cb();
    }
    
    //We eventiualy wait for keyboard return key
    if ((blc_status==BLC_PAUSE) || (blc_command_loop_period==-1))  BLC_PTHREAD_CHECK(pthread_mutex_lock(&mutex_lock_keyboard), NULL);
    else if (blc_status==BLC_RUN){
        //We wait for semaphore
        FOR(i, waiting_semaphores_nb) {
            blocking_semaphore=waiting_semaphores[i];
            SYSTEM_ERROR_CHECK(sem_wait(blocking_semaphore), -1, NULL);
        }
        blocking_semaphore=NULL;
            
        //Waiting general user defined callbacks
        FOR(i, waiting_callbacks_nb) waiting_callbacks[i](waiting_callabacks_user_data[i]);
    }
    else blc_command_loop_period=-3; //In case we start another loop after
    
    //We start counting the duration time
    if (intermediate_iteration==0) blc_us_time_diff(&timer);
    else  blc_period+=blc_current_duration + blc_us_time_diff(&timer);
    
    return blc_status;
}

void blc_command_loop_end(){
    div_t ratio;
    struct timespec time_left, tmp_rem;
    int diff, i;
    uint64_t start_time;

    if (blc_profile_file) start_time=timer.tv_sec*1000000+timer.tv_usec;
    blc_current_duration=blc_us_time_diff(&timer);
    if (blc_profile_file) fprintf(blc_profile_file, "%16ld\t%16lld\t%8lu\t\n",profile_timer.tv_sec*1000000+profile_timer.tv_usec, start_time, blc_current_duration);
 //   fflush(stderr);
    
    //We post the semaphore usualy to say the data is ready to read
    FOR(i, posting_semaphores_nb) SYSTEM_ERROR_CHECK(sem_post(posting_semaphores[i]), -1, NULL);
    
    blc_duration+=blc_current_duration;
    
    if (blc_current_duration<blc_duration_min) blc_duration_min=blc_current_duration;
    else if (blc_current_duration > blc_duration_max) blc_duration_max=blc_current_duration;
    
    if (blc_command_loop_period > 0){
        diff=blc_command_loop_period - blc_current_duration;
        ratio=div(diff, 1e6);
        time_left.tv_sec = ratio.quot;
        time_left.tv_nsec = ratio.rem*1000;
        if (diff  < 0) color_fprintf(BLC_YELLOW, stderr, "\r%s: Missing %.3fms in the BLC_COMMAND_LOOP requesting refresh of %.3fms\n", blc_program_id,  -diff/1000.f, blc_command_loop_period/1000.f);
        else SYSTEM_ERROR_RETRY_ON_SPECIFIC_ERRNO(nanosleep(&time_left, &tmp_rem), -1, EINTR, "Time left %.3fms", tmp_rem.tv_nsec/1e6f + tmp_rem.tv_sec*1000);
        /* Warning the nanosleep can be overtimed of 10ms !!! it is the same for usleep !! */
    }
    intermediate_iteration++;
    blc_loop_iteration++;
}

void *blc_loop_wait_stop(){
    void *result;
    SYSTEM_ERROR_CHECK(pthread_join(loop_thread, &result), -1, NULL);
    return result;
}
