/*Demonstration of blc_program API*/

#include "blc_program.h"

int main( int argc, char **argv){
    
    char const *text_option, *first_parameter, *optional_parameter, *flag;
    
    //The text that appear when help is called.
    blc_program_set_description("Program to show how to parse arguments.");
    
    //Define the options to be parsed with 'blc_program_option_interpret'.
    blc_program_add_option(&flag, 'f', "flag", NULL, "Show how to read a flag", NULL);
    blc_program_add_option(&text_option, 's', "simple", "string", "Simple text as option", "Default text");
  
    //Require one argument.
    blc_program_add_parameter(&first_parameter, "string", 1,  "Required parameter ", NULL);
    
    //Accept a second optional argument.
    blc_program_add_parameter(&optional_parameter, "string", 0,  "Show how to accept simple text as option", NULL);

    //Interprets the arguments, associates variables and print program name as title.
    blc_program_parse_args(&argc, &argv);
    
    //We display on stderr. It is a good habit to use for interaction with the user
    fprintf(stderr, "Display help:\n");
    
    //Display all the possible options / arguments
    blc_program_args_display_help();
    
    //After interpret options, we can use the variable **blc_program_name**
    fprintf(stderr, "Parsed arguments:\n");
    
    //We present the parsed variables
    fprintf(stderr, "- The simple option text is: '%s'\n", text_option);
    if (flag) fprintf(stderr, "- The flag is activated, its content (usually useless) is : '%s'\n", flag);
    else fprintf(stderr, "- The flag is not activated.\n");

    fprintf(stderr, "- The first argument is: '%s'\n", first_parameter);
    if (optional_parameter) fprintf(stderr, "- The optional argument is: '%s'\n", optional_parameter);
    else fprintf(stderr, "- No optional argument\n");

    return 0;
}
