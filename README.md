BLC program
===========

Copyright : [ETIS](http://www.etis.ensea.fr/neurocyber) - ENSEA, University of Cergy-Pontoise, CNRS (2011-2016)  
Author    : [Arnaud Blanchard](http://arnaudblanchard.thoughtsheet.com)  
Licence   : [CeCILL v2.1](http://www.cecill.info/licences/Licence_CeCILL_V2-en.html)  

### Library providing command line program facilities
- Parsing arguments
- Interacting with the user

### [Online documentation](https://framagit.org/blaar/blc_program/wikis/home)


